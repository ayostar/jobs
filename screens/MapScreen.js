import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Components } from 'expo';

class MapScreen extends Component {
  state = {
    region: {
      longitude: -122,
      latitude: 37,
      longitudeDelta: 0.04,
      latitudeDelta: 0.09
    }
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Components.MapView
          region={this.state.region}
          style={{ flex: 1 }}
          provider={Components.MapView.PROVIDER_GOOGLE}
        />
      </View>
    );
  }
}

export default MapScreen;
