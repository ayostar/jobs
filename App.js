import React, { Component } from 'react';
import { Constants } from 'expo';
import { StyleSheet, Text, View, Platform } from 'react-native';
import { TabNavigator, StackNavigator } from 'react-navigation';
import { Provider } from 'react-redux';
import store from './store';

import AuthScreen from './screens/AuthScreen';
import WelcomeScreen from './screens/WelcomeScreen';
import MapScreen from './screens/MapScreen';
import DeckScreen from './screens/DeckScreen';
import ReviewScreen from './screens/ReviewScreen';
import SettingScreen from './screens/SettingScreen';

class App extends Component {
  render() {
    const MainNavigator = TabNavigator(
      {
        welcome: { screen: WelcomeScreen },
        auth: { screen: AuthScreen },
        main: {
          screen: TabNavigator({
            map: { screen: MapScreen },
            deck: { screen: DeckScreen },
            review: {
              screen: StackNavigator({
                review: { screen: ReviewScreen },
                setting: { screen: SettingScreen }
              })
            }
          }, {
            tabBarPosition: 'bottom',
            swipeEnabled: false,
            animationEnabled: false
          })
        }
      },
      {
        navigationOptions: {
          tabBarVisible: false
        },
        lazy: true,
        swipeEnabled: false,
        animationEnabled: false,
        tabBarPosition: 'bottom'
      }
    );

    return (
      <Provider store={store}>
        <View style={styles.container}>
          <MainNavigator />
        </View>
      </Provider>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    marginTop: Platform.OS === 'android' ? Expo.Constants.statusBarHeight : undefined
  }
}

export default App;
